# Nodo Biofabricación Digital

El proyecto Nodo BioFabricación Digital consiste en el diseño, documentación y diseminación de una red de laboratorios creativos de Código Abierto para el suprarreciclaje de residuos orgánicos, que asisten la fabricación distribuida de biomateriales y bioproductos a partir de residuos procedentes de las redes de alimentación. El nodo es proyectado para ser ubicado cerca de donde se generan los residuos, integrándose al tejido social y agrícola de cada localidad, haciendo aparecer la diversidad territorial y amplificando su resiliencia. Cada laboratorio incorpora un set de herramientas y tecnologías de bajo costo y formato escritorio, para abordar diferentes procesos de biofabricación, tales como impresión 3d, rotomoldeado, y termoformado de biomateriales. Las máquinas funcionan intercalando operaciones digitales y análogas, con el fin y a diferencia de la producción industrial, de mantener embebido al usuario durante todo el proceso, el que es posible definir como una colaboración entre la máquina, el humano y los agentes naturales. 
 
El horizonte del proyecto es contribuir a generar una red Latinoamericana de diseño y fabricación distribuida basada en tecnologías flexibles, ciudadanos creativos y residuos endémicos. El propósito del proyecto es promover a través de la creatividad, las tecnologías y la inteligencia colectiva, una nueva cultura material para la autosuficiencia de los territorios, volviendo a dotar los objetos de una narrativa local y promoviendo modelos productivos regenerativos. 

Tecnologías desde el sur y para el sur!

<img src="IMG_5170__2_.jpg" width="400"> <img src="/IMG_5221__1_.jpg" width="400">
 
Este proyecto es liderado por el FabLab U. de Chile, financiado por el Ministerio de Culturas, Artes y Patrimonio de Chile, apoyado por la plataforma internacional [Materiom](https://materiom.org/), y desarrollado en colaboración con estudiantes e investigadores de la Universidad de Chile. 

## Tecnologías del Nodo Biofabricación Digital

- [Biomixer](https://gitlab.com/fablab-u-de-chile/biomixer): La Biomixer es una máquina de Control Numérico que permite dispensar, calentar y mezclar ingredientes con precisión, y funciona junto a una web, que calcula y ejecuta recetas a partir de porcentajes y cantidades. La biomixer es el corazón del Nodo, ya que es el punto de partida para los demás procesos; con ella se vinculan la bioformadora, la bio rotomoldeadora y la Impresora 3D de biomateriales. 

- [Bioprinter](https://gitlab.com/fablab-u-de-chile/3dp-materiales-pastosos): Impresora 3D para extruir biomateriales en formato pasta a temperatura ambiente. Se espera que el flujo de trabajo se inicie con la Biomixer, donde se generan mezclas precisas que luego se vierten en los contenedores de la Bioimpresora 3D.

- [Bioformadora](https://gitlab.com/fablab-u-de-chile/termoformadora): Termoformadora para biomateriales que funciona con calor refractario o vapor de agua para moldear láminas de biomateriales sobre figuras sólidas. El flujo de trabajo se inicia con la Biomixer, donde se generan mezclas precisas que luego se vierten en bastidores con el tamaño necesario. La lámina debe estar deshidratada al momento de termoformar y no superar los 400x400mm.  

- [Rotomoldeadora](https://gitlab.com/fablab-u-de-chile/rotomoldeadora): Máquina rotomoldeadora para fabricar geometrías cerradas y huecas de biomaterial. Es importante considerar que la deshidratación del biomaterial posterior al rotomoldeado hará que disminuya el tamaño de la geometría obtenida. El flujo de trabajo se inicia con la Biomixer, donde se generan mezclas precisas que luego se vierten en el molde que girará de forma controlada. 

- [Compostera](https://gitlab.com/fablab-u-de-chile/biocompostera): Pequeña compostera DIY, orientada a medir y obtener data de los procesos de biodegradación. Cuenta con triturador y monitoreo de humedad.

Descarga el Toolkit del Nodo Biofabricación Digital, donde por cada tecnología podras encontrar descripción, modelos 3D, partes y piezas, rol dentro del nodo y trabajo futuro:

https://drive.google.com/file/d/1Gkk_uVqlyh64f83y1MJnm_LeAaLpLW9M/view?usp=sharing

### Contáctanos

- [Página Web](http://www.fablab.uchile.cl/)
- [Instagram](https://www.instagram.com/fablabudechile/?hl=es-la)
- [Youtube](https://www.youtube.com/channel/UC4pvq8aijaqn5aN02GiDwFA)
